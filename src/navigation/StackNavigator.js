import React, { useContext } from 'react';
import { View, Text } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import AuhtContext from '../context/AuthContext';
import Login from '../screens/Login';
import Register from '../screens/Register';
import Home from '../screens/Home';
import Products from '../screens/Products';
import Location from '../screens/Locations';
import Detail from '../screens/Detail';

const Stack = createStackNavigator();

const StackNavigator = () => {
  const { isSignedIn } = useContext(AuhtContext);

  return (
    <Stack.Navigator>
      {isSignedIn ? (
        <>
          <Stack.Screen
            name="Home"
            component={Home}
            options={{ 
              title: 'Mercado Preso', 
              headerStyle: {
                backgroundColor: "#1dc4d8"
            }}}
          />
          <Stack.Screen
            name="Products"
            component={Products}
            options={({ route }) => ((
              {
                title: route.params.title,
                headerStyle: {
                  backgroundColor: "#1dc4d8"
                }
              }
            ))}
          />
          <Stack.Screen
            name="Location"
            component={Location}
            options={{ 
              title: 'Ubicación', 
              headerStyle: {
                backgroundColor: "#1dc4d8"
            }}}
          />
          <Stack.Screen
            name="Detail"
            component={Detail}
            options={{ 
              title: 'Comentarios', 
              headerStyle: {
                backgroundColor: "#1dc4d8"
            }}}
          />
        </>
      ) : (
        <>
          <Stack.Screen
            name="Login"
            component={Login}
            options={{ 
              title: 'Inicio de sesión', 
              headerStyle: {
                backgroundColor: "#a4d8fe"
              }}}
          />
          <Stack.Screen
            name="Register"
            component={Register}
            options={{ 
              title: 'Registro de usuarios', 
              headerStyle: {
                backgroundColor: "#a4d8fe"
              }}}
          />
        </>
      )}
    </Stack.Navigator>
  );
};

export default StackNavigator;
