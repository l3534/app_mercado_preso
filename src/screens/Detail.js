import React, { useState, useEffect } from 'react';
import { View, FlatList, Text, StyleSheet } from 'react-native';
import { useDispatch, useSelector } from "react-redux"
import { fetchAllComments } from '../store/slices/comments';
import CommentListItem from '../components/CommentListItem';
import FormComents from '../components/FormComents';
import useFetch from '../hooks/Fetch';

const Detail = ({ route }) => {
  const [loading, setLoading] = useState(true);

  const { title, body, products_id, navigation } = route.params;

  const dispatch = useDispatch();
  const  { comments } = useSelector((state) => state.comments.list)

  useEffect(() => {
    dispatch(fetchAllComments(products_id))
    setLoading(false);
  }, [dispatch, products_id]);

  return (
    <View style={styles.container}>
      {loading ? (
        <Text>Cargando...</Text>
      ) : (
        <>
          <FormComents id={products_id}/>
          <Text style={styles.text}>Comentarios</Text>
          <FlatList
            keyExtractor={(x) => x.id}
            data={comments}
            renderItem={({ item }) => (
              <CommentListItem
                title={item.title}
                body={item.body}
                comments={comments}
              />
            )}
          />
        </>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  text: {
    alignSelf: 'center',
    fontSize: 18,
    marginVertical: 10,
  },
  container: {
    flex: 1,
  },
});

export default Detail;
