import React, { useEffect, useState } from 'react';
import { View, Text, StyleSheet, FlatList } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import useFetch from '../hooks/Fetch';
import { fetchAllShops } from '../store/slices/shops';
import ShopListItem from '../components/ShopListItem';

const Home = ({ navigation }) => {
  const [loading, setLoading] = useState(true);
  const { list: data } = useSelector((state) => state.shops);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchAllShops());
    setLoading(false);
  }, [dispatch]);

  return (
    <View style={styles.container}>
      {loading ? (
        <Text style={styles.loading}>Cargando...</Text>
      ) : (
        <>
          <Text style={styles.title}>~ Tiendas disponibles ~</Text>
          <FlatList
            style={styles.list}
            keyExtractor={(x) => x.id}
            data={data}
            renderItem={({ item }) => (
              <ShopListItem
                onPress={() =>
                  navigation.navigate('Products', {
                    title: item.name,
                    coord: item.coords,
                    desc: item.desc,
                    products: item.products,
                  })
                }
                text={item.name}
                desc={item.desc}
                image={item.imageUrl}
              />
            )}
          />
        </>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
  },
  list: {
    alignSelf: 'stretch',
  },
  title: {
    fontSize: 20,
    paddingVertical: 15,
  },
  loading: {
    fontSize: 26,
  },
});

export default Home;
