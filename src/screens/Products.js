import React from 'react';
import { View, Text, StyleSheet, FlatList } from 'react-native';
import ProductListItem from '../components/ProductListItem';
import CustomButtom from '../components/CustomButton';

const Products = ({ route, navigation }) => {
  const { coord, desc, title, products } = route.params;
  
  return (
    <View style={styles.container}>
      <Text style={styles.title}>~ Listado de productos ~</Text>
      <FlatList
        style={styles.list}
        keyExtractor={(x) => x.id}
        data={products}
        renderItem={({ item }) => (
          <ProductListItem
            name={item.name}
            desc={item.desc}
            image={item.imageUrl}
            onPress={() =>
                navigation.navigate('Detail', {
                title: item.title,
                body: item.body,
                products_id: item._id,
              })
            }
          />
        )}
      />
      <View style={styles.footer}>
        <CustomButtom
          onPress={() =>
            navigation.navigate('Location', {
              coordi: coord,
              des: desc,
              title: title,
            })
          }>
          Ver ubicaciones
        </CustomButtom>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  title: {
    fontSize: 20,
    alignSelf: 'center',
    paddingVertical: 15,
  },
  list: {
    paddingHorizontal: 10,
  },
  footer: {
    padding: 10,
  }
});

export default Products;
