import React, { useContext } from 'react';
import { View, StyleSheet, Image } from 'react-native';
import { Formik } from 'formik';
import * as Yup from 'yup';
import AuthContext from '../context/AuthContext';
import Input from '../components/Input';
import CustomButton from '../components/CustomButton';


const Login = ({ navigation }) => {
  const { login } = useContext(AuthContext);

  return (
    <View style={styles.container}>
      <View>
        <Image
          style={styles.image}
          source={require('../assets/team-meta.png')}
        />
        <Formik
          initialValues={{ email: '', password: '' }}
          onSubmit={login}
          validationSchema={Yup.object({
            email: Yup.string()
              .required('El campo es requerido')
              .email('El email no es valido'),
            password: Yup.string().required('El campo es requerido'),
          })}>
          {({ handleSubmit }) => (
            <View style={styles.center}>
            <Input name="email" placeholder="Email" autoCapitalize="none" />
            <Input
              name="password"
              placeholder="Password"
              autoCapitalize="none"
              secureTextEntry={true}
              onSubmitEditing={handleSubmit}
            />
            <View style={styles.buttons}>
              <CustomButton onPress={handleSubmit}>Enviar</CustomButton>
              <CustomButton onPress={() => navigation.navigate('Register')}>
                Registrarse
              </CustomButton>
            </View>
          </View>
          )}
        </Formik>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    height: 70,
    width: 250,
    marginBottom: 25,
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  buttons: {
    width: 140,
    alignSelf: 'center',
    marginTop: 10,
  },
  center: {
    alignSelf: 'center',
  },
});

export default Login;
