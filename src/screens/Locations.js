import React from 'react';
import { View, Text, StyleSheet, Dimensions } from 'react-native';
import MapView, { Marker } from 'react-native-maps';

const Locations = ({ route, navigation }) => {
  const { coordi, des, title } = route.params;

  return (
    <View style={styles.container}>
      <MapView
        style={styles.map}
        initialRegion={{
          latitude: 20.66682,
          longitude: -103.39182,
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421,
        }}>
        {coordi.map((coord) => (
          <Marker
            coordinate={{
              latitude: Number(coord.latitude),
              longitude: Number(coord.longitude),
            }}
            pinColor={'purple'}
            title={title}
            description={des}
          />
        ))}
      </MapView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
  },
  map: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },
});

export default Locations;
