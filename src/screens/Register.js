import React from 'react';
import { Formik, useFormikContext } from 'formik';
import * as Yup from 'yup';
import Input from '../components/Input';
import { View, StyleSheet, Image, Alert } from 'react-native';
import CustomButton from '../components/CustomButton';

const Register = ({ navigation }) => {
  const registerUser = async (values) => {
    const url =
      'https://api-mercado-preso-eberpaz33.vercel.app/api/auth/register';

    const response = await fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(values),
    });
    if (response.status === 201) {
      return Alert.alert('Exito', await response.text(), [
        { text: 'Iniciar sesión', onPress: () => navigation.goBack() },
      ]);
    }
    Alert.alert('Error, algo salió mal', await response.text());
  };

  const EmailForm = () => {
    const { submitForm } = useFormikContext();
    return (
      <>
        <Input name="email" placeholder="Email" autoCapitalize="none" />
        <Input
          name="password"
          placeholder="Password"
          autoCapitalize="none"
          secureTextEntry={true}
          onSubmitEditing={submitForm}
        />
        <View style={styles.buttons}>
          <CustomButton onPress={submitForm}>Enviar</CustomButton>
          <CustomButton onPress={() => navigation.goBack()}>
            Iniciar sesión
          </CustomButton>
        </View>
      </>
    );
  };

  return (
    <View style={styles.container}>
      <View>
        <Image
          style={styles.image}
          source={require('../assets/team-meta.png')}
        />
        <Formik
          onSubmit={registerUser}
          validationSchema={Yup.object({
            email: Yup.string()
              .email('Correo invalido')
              .required('El campo es requerido'),
            password: Yup.string().required('El campo es requerido'),
          })}
          initialValues={{ email: '', password: '' }}>
          <EmailForm />
        </Formik>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    height: 70,
    width: 250,
    marginBottom: 25,
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  buttons: {
    width: 140,
    alignSelf: 'center',
    marginTop: 10,
  },
});

export default Register;
