import React, { useState } from 'react';

const validate = (values) => {
  let validate = true;
  let errors = {};
  const validateEmail =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

  if (!values.email) {
    errors.email = 'El campo es requerido';
    validate = false;
  } else if (!validateEmail.test(values.email)) {
    validate = false;
    errors.email = 'Introduzca un correo valido';
  }

  if (!values.password) {
    validate = false;
    errors.password = 'El campo es requerido';
  }

  if (validate) {
    errors.validate = true;
  }

  return errors;
};

const useForm = (initialValues, onSubmit) => {
  const [formValues, setFormValues] = useState(initialValues);
  const [formErrors, setFormErrors] = useState({});

  const handleChange = (value, name) => {
    setFormValues({ ...formValues, [name]: value });
  };

  const handleSubmit = () => {
    setFormErrors(validate(formValues))

    if (validate(formValues).validate) {
      onSubmit(formValues)
    }
  };

  return [formValues, formErrors, handleChange, handleSubmit];
};

export default useForm;
