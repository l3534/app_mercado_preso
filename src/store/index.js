import { configureStore } from '@reduxjs/toolkit';
import shops from './slices/shops';
import comments from './slices/comments';

export default configureStore({
  reducer: {
    shops,
    comments,
  },
});
