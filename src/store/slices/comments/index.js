import { createSlice } from '@reduxjs/toolkit';

export const commentsSlce = createSlice({
  name: 'comments',
  initialState: {
    list: [],
  },
  reducers: {
    setCommentList: (state, action) => {
      state.list = action.payload;
    },
    newComment: (state, action) => {
      state.list.comments = [action.payload].concat(state.list.comments)
    }
  },
});

export default commentsSlce.reducer;

const { setCommentList, newComment } = commentsSlce.actions;

export const fetchAllComments = (id) => async (dispatch) => {
  const response = await fetch(
    `https://api-mercado-preso-eberpaz33.vercel.app/api/products/${id}`
  );
  const data = await response.json();
  dispatch(setCommentList(data));
};

export const concatComment = (comment) => (dispatch) => {
  dispatch(newComment(comment))
}
