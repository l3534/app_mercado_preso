import { createSlice } from '@reduxjs/toolkit';

export const shopsSlice = createSlice({
  name: 'shops',
  initialState: {
    list: [],
  },
  reducers: {
    setShopList: (state, action) => {
      state.list = action.payload;
    },
  },
});

const { setShopList } = shopsSlice.actions;

export default shopsSlice.reducer;

export const fetchAllShops = () => async (dispatch) => {
  const response = await fetch(
    'https://api-mercado-preso-eberpaz33.vercel.app/api/shops'
  );
  const data = await response.json();
  dispatch(setShopList(data));
};
