import React, { createContext, useState } from 'react';

const AuthContext = createContext();

export default AuthContext;

export const AuthProvider = ({ children }) => {
  const [isSignedIn, setIsSignedIn] = useState(false);
  const [token, setToken] = useState({});

  const login = async (values) => {
    const url = 'https://api-mercado-preso-eberpaz33.vercel.app/api/auth/login';

    const response = await fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(values),
    });

    const data = await response.text();

    try {
      const userToken = JSON.parse(data);
      setToken(userToken);
      alert('Sesión iniciada correctamente');
      setIsSignedIn(true);
    } catch {
      alert(data);
    }
  };

  const authData = {
    isSignedIn: isSignedIn,
    login: login,
    token: token,
  };

  return (
    <AuthContext.Provider value={authData}>{children}</AuthContext.Provider>
  );
};
