import React from 'react';
import { View, Text, StyleSheet, Image, Dimensions } from 'react-native';
import CustomButtom from '../components/CustomButton';

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#EBEEF8',
    marginBottom: 30,
    borderRadius: 15,
  },
  image: {
    width: '100%',
    height: 500,
    resizeMode: 'stretch',
    borderTopRightRadius: 15,
    borderTopLeftRadius: 15,
  },
  containerImage: {
    width: '100%',
  },
  footer: {
    padding: 20,
  },
  title: {
    fontSize: 25,
    fontWeight: "bold",
    alignSelf: "center",
    marginBottom: 15,
  },
  description: {
    fontSize: 18,
  }
});

const ProductListItem = ({ name, image, desc, onPress }) => {
  return (
    <View style={styles.container}>
      <View style={styles.containerImage}>
        <Image style={styles.image} source={{ uri: image }} />
      </View>
      <View style={styles.footer}>
        <Text style={styles.title}>{name}</Text>
        <Text style={styles.description}>{desc}</Text>
        <CustomButtom
          onPress={onPress}>
          Comentarios
        </CustomButtom>
      </View>
    </View>
  );
};

export default ProductListItem;
