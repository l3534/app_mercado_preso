import { TextInput, StyleSheet, Text } from 'react-native';
import React from 'react'
import { useField } from 'formik';

const Input = ({ name, option, ...props }) => {
  const [field, meta] = useField(name);
    return (
      <>
        <TextInput
          style={styles.input}
          onChangeText={field.onChange(name)}
          onBlur={field.onBlur(name)}
          value={field.value}
          {...props}
        />
        {meta.error && meta.touched && (
          <Text style={styles.error}>{meta.error}</Text>
        )}
      </>
    );
  };

export default Input;

const styles = StyleSheet.create({
  input: {
    borderColor: '#ffbfdb',
    borderWidth: 1.5,
    height: 35,
    width: 300,
    fontSize: 14,
    paddingHorizontal: 10,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 5,
  },
  error: {
    color: '#FF3933',
    fontSize: 14,
    left: 5,
  },
});