import React from 'react';
import { Image, Text, StyleSheet, View, Button } from 'react-native';
import CustomButton from './CustomButton';

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 10,
    backgroundColor: '#EBEEF8',
    marginBottom: 10,
    borderBottomWidth: 3,
    borderBottomColor: '#FF6B33',
    padding: 8,
    borderRadius: 8,
  },
  image: {
    width: 50,
    height: 50,
    borderRadius: 50,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  name: {
    fontSize: 18,
    paddingLeft: 15,
  },
  button: {
    width: 130,
    alignSelf: 'flex-end',
    marginTop: 15,
  },
  desc: {
    fontSize: 15,
    marginTop: 15,
  },
});

const ShopListItem = ({ onPress, text, desc, image }) => {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Image style={styles.image} source={{ uri: image }} />
        <Text style={styles.name}>{text}</Text>
      </View>
      <Text style={styles.desc}>{desc}</Text>
      <View style={styles.button}>
        <CustomButton onPress={onPress}>Ver productos</CustomButton>
      </View>
    </View>
  );
};

export default ShopListItem;
