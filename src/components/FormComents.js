import React from "react";
import { View, Text, StyleSheet } from "react-native";
import { Formik } from "formik";
import * as Yup from "yup";
import { useDispatch } from "react-redux";
import { concatComment } from "../store/slices/comments";
import Input from "./Input";
import CustomButton from "./CustomButton";

const FormComents = ({ id }) => {
  const dispatch = useDispatch();

  const Submit = async (values, resetForm) => {
    const body = {
      title: values.title,
      body: values.body,
    };
    const url = "https://api-mercado-preso-eberpaz33.vercel.app/api/comments";

    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(body),
    });
    const data = await response.json();

    if (response.status === 201) {
      const url2 = `https://api-mercado-preso-eberpaz33.vercel.app/api/products/assignComment/${id}`;

      const response2 = await fetch(url2, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ comment: data._id }),
      });

      if (response2.status === 204) {
        dispatch(concatComment(values));
        resetForm();
        return alert("Comentario subido con exito");
      }
    }
    alert("Hubo un error, intente más tarde");
  };

  return (
    <View style={styles.container}>
      <View style={styles.center}>
        <Text style={styles.title}> ~ Agregar comentario ~ </Text>
        <Formik
          initialValues={{}}
          validationSchema={Yup.object({
            title: Yup.string().required("El campo es requerido"),
            body: Yup.string().required("EL campo es requerido"),
          })}
          onSubmit={(values, { resetForm }) => Submit(values, resetForm)}
        >
          {({ handleSubmit }) => (
            <>
              <Input name="title" placeholder="Agregar titulo de comentario" />
              <Input
                style={styles.inputBody}
                name="body"
                placeholder="Agregar comentario"
                multiline={true}
                numberOfLines={5}
              />
              <CustomButton onPress={handleSubmit}>
                Subir comentario
              </CustomButton>
            </>
          )}
        </Formik>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignSelf: "center",
  },
  inputBody: {
    borderColor: "#ffbfdb",
    borderWidth: 1.5,
    width: 300,
    fontSize: 14,
    paddingHorizontal: 10,
    borderRadius: 10,
    marginVertical: 5,
    textAlignVertical: "top",
    paddingTop: 5,
  },
  title: {
    alignSelf: "center",
  },
});
export default FormComents;
