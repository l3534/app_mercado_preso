import React from 'react';
import { Text, View, StyleSheet } from 'react-native';

const CommentListItem = ({ title, body }) => {
  return (
    <View>
      <View style={styles.comment}>
        <Text style={styles.title}>{title}</Text>
        <Text style={styles.body}>{body}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  comment: {
    borderWidth: 6,
    borderStyle: 'dotted',
    marginTop: 15,
    borderColor: '#1dc4d8'
  },
  title: {
    flex: 1,
    alignSelf: 'center',
    fontSize: 18,
    color: '#2f719a',
    fontWeight: '600',
  },
  body: {
    margin: 12,
    textAlign: 'justify'
  }
})

export default CommentListItem;